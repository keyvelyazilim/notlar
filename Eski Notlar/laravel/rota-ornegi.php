<?php
/**
 * Created by PhpStorm.
 * User: atillao
 * Date: 02.05.2017
 * Time: 00:29
 */

Route::resource('admin/pages/', 'BackPagesController', ['names'=>[

    'index' => 'admin.page.index',
    'store' => 'admin.page.store',
    'create' => 'admin.page.create',
    'show' => 'admin.page.show',
    'update' => 'admin.page.update',
    'destroy' => 'admin.page.destroy',
    'edit' => 'admin.page.edit'

]]);