<?php
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () {
 Route::get('/new','RecordsController@showNew');

 Route::get('/view','RecordsController@showAll');
 Route::get('/edit','RecordsController@showEdit');
 Route::get('edit/{id}','RecordsController@edit');
 Route::post('edited_data/{id}','RecordsController@update');

 Route::get('/uyari2',function(){
        session()->flash('msg','Yaw, ati kayıt eklerken neden uyarı çıkarmıyor bu ibne Laravel?');
        return redirect()->to('/');
  });
  

  
});

 Route::post('/info','RecordsController@store');

//http://atilla.dev/atilla-abi/biseydiycem
Route::get('atilla-abi/{bisey}', function($bisey){
return 'olum canım çok sıkkın bana '.$bisey. ' deneyin';
});

Route::get('/uyari',function(){
        session()->flash('msg','Hey, You have a message to read');
        return redirect()->to('/');
});

 Route::get('/uyari3', function() {
    \Session::flash('msg', 'Changes Saved.' ); 
     return redirect()->to('/');
 });