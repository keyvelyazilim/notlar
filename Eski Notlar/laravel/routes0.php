<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/view','RecordsController@showAll');

Route::group(['middleware' => ['web']], function () {
    Route::get('/new','RecordsController@showNew');
    Route::post('/info','RecordsController@store');
    Route::get('/edit','RecordsController@showEdit');
});

//http://ecms.dev/deneme
Route::get('deneme', function(){
return 'deneme sayfası';
});

//http://ecms.dev/deneme/icerikler
Route::get('deneme/icerikler', function(){
return 'deneme sayfası icerikler';
});

//http://ecms.dev/deneme/... değişkenden gelsin
Route::get('deneme/{kategori}', function($kategori){
return 'deneme sayfası '.$kategori. ' icerikler';
});

http://atilla.dev/atilla-abi/biseydiycem
Route::get('atilla-abi/{bisey}', function($bisey){
return 'olum canım çok sıkkın bana '.$bisey. ' deneyin';
});

//http://ecms.dev/deneme/parametre/... kaçıncı parametre olursa olsun değişken gelsin.
Route::get('deneme/parametre/{kategori}', function($kategori){
return "deneme sayfası > {$kategori} < istediğimiz parametre";
});